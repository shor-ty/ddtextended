### Time derivative calculation function library###
This function library is an extension the the ddt function build in the OpenFOAM Foundation version.
The extension to the standard buildin version is simply:
- Allowing the execution of the ddt using the postProcess application

### Discussion ###
The postProcess functionality is not used in the official sources based on inconsistency of the time
derivative calculation as discussed here:
https://bugs.openfoam.org/view.php?id=2525

### OpenFOAM Versions ###
- 5.x

### How to Install ###
- Clone the repository
> git clone https://shor-ty@bitbucket.org/shor-ty/ddtextended.git
- Go into the repository
> cd ddtextended
- Checkout the repository you need
> git checkout OpenFOAM-5.x
- Copy the whole stuff to your $WM\_PROJECT\_DIR dir (attension! overwriting FOAM files)
> cp -r * $WM\_PROJECT\_DIR
- Open the files file in the object library
> vim $WM\_PROJECT\_DIR/src/functionObjects/fields/Make/files
- Add the following line above the last line
> ddt/ddt.C
- Go to the folder $WM\_PROJECT\_DIR/src/functionObjects/fields and build the new function
> wmake libso
- Enjoy

### How to Use ###
- fvOptions: An example is given in the header file
- postProcess:
> postProcess -func 'ddt(p)'

### Contributor ###
- Tobias Holzmann
